cmake_minimum_required(VERSION 3.5)

set(PROJECT_NAME ark_touch)
project(${PROJECT_NAME})

## @TODO(stdmatt): This is hackyyy!! We need to find a better way to specify the
## compiler toolchain for dos without making all this things on every CMakeLists.txx
## 5/2/2021, 10:40:13 PM
option(BUILD_FOR_DOS "Building for DOS??" OFF) #OFF by default
if(BUILD_FOR_DOS)
    set (CMAKE_SYSTEM_NAME linux-djgpp)
    set (DJGPP TRUE)
    # specify the cross compiler
    set (CMAKE_C_COMPILER   /usr/local/bin/i386-pc-msdosdjgpp-gcc)
    set (CMAKE_CXX_COMPILER /usr/local/bin/i386-pc-msdosdjgpp-g++)
    # search for programs in the build host directories
    set (CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    # for libraries and headers in the target directories
    set (CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set (CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set (CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

    set (CMAKE_AR:FILEPATH      /usr/local/bin/i386-pc-msdosdjgpp-ar)
    set (CMAKE_RANLIB:FILEPATH /usr/local/bin/i386-pc-msdosdjgpp-ranlib)
endif(BUILD_FOR_DOS)
unset(BUILD_FOR_DOS CACHE)


##----------------------------------------------------------------------------##
## Compiler Definitions                                                       ##
##----------------------------------------------------------------------------##
set(CMAKE_CXX_STANDARD          14)
set(CMAKE_CXX_STANDARD_REQUIRED on)

##----------------------------------------------------------------------------##
## ark_core                                                                   ##
##----------------------------------------------------------------------------##
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/libs/ark_core)


##----------------------------------------------------------------------------##
## Project                                                                    ##
##----------------------------------------------------------------------------##
file(GLOB_RECURSE
  PROJECT_SOURCE_FILES
  LIST_DIRECTORIES false
  ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}/**
)

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)
set(PROJECT_FILES
    ${PROJECT_SOURCE_FILES}
)

##----------------------------------------------------------------------------##
## Executable Definitions                                                     ##
##----------------------------------------------------------------------------##
add_executable(${PROJECT_NAME} ${PROJECT_FILES})
target_link_libraries(${PROJECT_NAME} PUBLIC ark_core)
